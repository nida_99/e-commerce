import React from "react";
import JwelleryCategories from "./JewelleryCategories";

class Start extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            productArr: [],
            isDataFetched: false,
            error: false,
            loadingData: true
        }
    }

    componentDidMount = () => {
        fetch('https://fakestoreapi.com/products')
            .then(res => res.json())
            .then(data => {
                this.setState({
                    productArr: data,
                    isDataFetched: true,
                    loadingData: false,
                    error: false
                })
            }).catch((err) => {
                this.setState({
                    error: true,
                    loadingData: false
                })
            })
    }
    render() {
        console.log(this.state.productArr)
        return (
            <>
                <div>
                    <JwelleryCategories array={this.state.productArr} />

                </div>


            </>
        )
    }


}


export default Start;