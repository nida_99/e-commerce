import React from 'react';
import { Link } from 'react-router-dom'
import Cssloader from "./Cssloader";
import HomeNav from './HomeNav';
import NewProduct from './NewProduct';
import SingleProduct from './SingleProduct';



class GadgetsCategories extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            gadgetArr: [],
            isDataFetched: false,
            error: false,
            loadingData: true
        }
    }

    componentDidMount = () => {
        fetch("https://fakestoreapi.com/products/category/electronics")
            .then(res => res.json())
            .then((data) => {
                console.log(data);
                this.setState({
                    gadgetArr: data,
                    isDataFetched: true,
                    loadingData: false,
                    error: false
                })
            }).catch((err) => {
                this.setState({
                    error: true,
                    loadingData: false
                })
            })
    }
    render() {

        return (
            <>
                <HomeNav />
                {/* <div className="home-nav-prod">
                    <ul>
                        <li> <Link className="link" to='/'> HOME </Link> </li>
                        <li><Link className="link" to='/products'> PRODUCT</Link></li>
                        <li><Link className="link" to='/collections'> COLLECTIONS</Link></li>
                    </ul>
                </div> */}
                {(this.state.loadingData === true) ? <Cssloader /> :

                    (this.state.error === true) ? <h1> Error occured!</h1>
                        :
                        <div className='whole-container'>
                            {this.state.gadgetArr.map((ele) => {

                                return (
                                    <div key={ele.id}>
                                        <SingleProduct src={ele.image} description={ele.title} price={ele.price} id={ele.id} rate={ele.rating.rate} count={ele.rating.count} />
                                    </div>
                                )

                            })}
                            <NewProduct />
                        </div>

                }

            </>)

    }

}
export default GadgetsCategories;
