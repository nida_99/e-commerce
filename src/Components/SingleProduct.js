
import React from 'react';
import './style.css'
class SingleProduct extends React.Component {
    render() {
        return (
            <>
                <div key={this.props.id} className='product-details'>
                    <div className='image-product'>
                        <img src={this.props.src} />
                    </div>
                    <div className='product-title'>
                        {this.props.description}
                    </div>

                    <div className='product-price'>
                        $ {this.props.price}
                    </div>

                    <div className='rating'>
                        <div>
                            {this.props.rate} <span>star</span>

                        </div>
                    </div>
                    <div>
                        <button className='cart-btn'>Add to cart</button>
                    </div>
                </div>


            </>
        )
    }
}
export default SingleProduct;