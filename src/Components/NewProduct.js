import React from 'react';
import './style.css'
import Cssloader from "./Cssloader";
import HomeNav from './HomeNav';
import { Link } from 'react-router-dom'
import SingleProduct from './SingleProduct';

class NewProduct extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            newProd: [],
            isDataFetched: false,
            error: false,
            loadingData: true
        }
    }

    componentDidMount = () => {
        fetch('https://fakestoreapi.com/products', {
            method: "POST",
            headers: {
                'Accept': 'application/json, text/plain,  ',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(
                {
                    title: 'test product',
                    price: 13.5,
                    description: 'lorem ipsum set',
                    image: 'https://i.pravatar.cc',
                    category: 'electronic'
                },

            )
        })
            .then(res => res.json())
            .then(newData => {
                console.log(newData)
                this.setState({
                    newProd: newData,
                    loadingData: false,
                    error: false,
                    isDataFetched: true

                })
            }).catch((err) => {
                console.log(err)
            })
    }

    render() {
        // console.log(Object.keys(this.state))
        return (
            <>

                {(this.state.loadingData === true) ? <Cssloader /> :

                    (this.state.error === true) ? <h1> Error occured!</h1>

                        :

                        <div className='whole-container'>

                            <>
                                <div className='single-product'>
                                    <SingleProduct src={this.state.newProd.image} description={this.state.newProd.title} price={this.state.newProd.price} id={this.state.newProd.id} rate={this.state.newProd.rating ? this.state.newProd.rating.rate : 0} />
                                </div>
                            </>


                        </div>

                }



            </>
        )





    }

}
export default NewProduct;