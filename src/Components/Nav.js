import React from 'react'
import './style.css'
import { Link } from 'react-router-dom'
class Nav extends React.Component {

    render() {
        return (
            <>
                <div className='nav-container'>
                    <div className='left-side'>
                        <ul>

                            <Link className='link-about' to="/aboutus"> <li> ABOUT US </li> </Link>

                            <Link className='link-about' to="/ourwork"> <li> OUR WORK </li> </Link>
                        </ul>

                    </div>
                    <div className='right-side'>

                        <ul>
                            <li className='rightborder'>Log in</li>
                            <li > Create Account</li>

                        </ul>
                    </div>
                </div>
            </>
        )
    }
}

export default Nav