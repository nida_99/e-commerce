
import React from 'react';
import { Link } from 'react-router-dom'
import Cssloader from "./Cssloader";
import HomeNav from './HomeNav';

import SingleProduct from './SingleProduct';




class WomenCategories extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            womenProdArr: [],
            isDataFetched: false,
            error: false,
            loadingData: true
        }
    }

    componentDidMount = () => {
        fetch("https://fakestoreapi.com/products/category/women's clothing")
            .then(res => res.json())
            .then((data) => {
                console.log(data);
                this.setState({
                    womenProdArr: data,
                    isDataFetched: true,
                    loadingData: false,
                    error: false
                })
            }).catch((err) => {
                this.setState({
                    error: true,
                    loadingData: false
                })
            })
    }


    render() {

        return (
            <>
                <HomeNav />
                {(this.state.loadingData === true) ? <Cssloader /> :

                    (this.state.error === true) ? <h1> Error occured!</h1>
                        :
                        <div className='whole-container'>
                            {this.state.womenProdArr.map((ele) => {

                                return (
                                    <div key={ele.id}>
                                        <SingleProduct src={ele.image} description={ele.title} price={ele.price} id={ele.id} rate={ele.rating.rate} count={ele.rating.count} />
                                    </div>
                                )

                            })}

                        </div>

                }

            </>)

    }

}
export default WomenCategories;
