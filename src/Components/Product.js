import React from 'react';
import './style.css'
import Cssloader from "./Cssloader";
import HomeNav from './HomeNav';
import { Link } from 'react-router-dom'
import NewProduct from './NewProduct';
import SingleProduct from './SingleProduct';
import MenCategories from './MenCategories';


class Product extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            productArr: [],
            isDataFetched: false,
            error: false,
            loadingData: true
        }
    }

    componentDidMount = () => {
        fetch('https://fakestoreapi.com/products')
            .then(res => res.json())
            .then(data => {
                console.log(data);
                this.setState({
                    productArr: data,
                    isDataFetched: true,
                    loadingData: false,
                    error: false
                })
            }).catch((err) => {
                this.setState({
                    error: true,
                    loadingData: false
                })
            })
    }


    render() {
        console.log("state", this.state.productArr)


        return (
            <div>
                {this.state.loadingData === true ? (
                    <Cssloader />
                ) : (
                    <MenCategories arr={this.state.productArr} />
                )}
            </div>
        );
        // return (
        //     <>
        //         {/* <HomeNav /> */}
        //         <div className='all-prod'>ALL PRODUCTS</div>
        //         {(this.state.loadingData === true) ? <Cssloader /> :

        //             (this.state.error === true) ? <h1> Error occured!</h1>

        //                 :

        //                 <div className='whole-container'>

        //                     {this.state.productArr.map((ele) => {

        //                         return (
        //                             <>
        //                                 <div className='single-product' key={ele.id}>
        //                                     <SingleProduct src={ele.image} description={ele.title} price={ele.price} id={ele.id} rate={ele.rating.rate} count={ele.rating.count} />

        //                                 </div>
        //                             </>

        //                         )

        //                     })}

        //                     <NewProduct />
        //                     <MenCategories arr={this.state.productArr} />

        //                 </div>

        //         }



        //     </>
        // )
    }

}
export default Product;