import React from 'react';
import Announcement from './Announcement';
import Footer from './Footer';
import HomeNav from './HomeNav';
import { Link } from 'react-router-dom'
import './style.css'

class OurWork extends React.Component {
    render() {

        return (
            <>
                <HomeNav />

                <div className='achievement'>OUR WORK</div>
                <div className='achievement-container'>

                    We are proud to announce that we have been honoured with a 2019 CO Leadership Award for Sustainable Fashion. As a long-term member of Common Objective, we strongly identify with the need to support and encourage sustainable fashion, a practice that we have been promoting for over 12 years.
                    The inaugural CO Leadership Awards launched in October 2018 to put the spotlight on businesses that are changing the way fashion is done - for the better.  Over 700 inspirational businesses applied, from every part of the fashion industry supply chain, and all over the world.
                    The goals for the awards are aligned with the mission of Common Objective. They want to support and incentivise any business, anywhere, to start to operate more sustainably. And they want to put the spotlight on best practice, inspiring and motivating others to follow suit.

                    The winners of Australia's Good Design Awards, the highest honour for design innovation in Australia, were announced at the Sydney Opera House on 17 May at the 60th Annual Good Design Awards Ceremony.
                    Barrel received a prestigious Good Design Award® Winner in the Fashion Design category in recognition for outstanding design and innovation.
                    The annual Good Design Awards is Australia¹s most prestigious Awards for design and innovation with a proud history dating back to 1958. The Awards celebrate the best new products and services on the market, excellence in architectural design, digital and communication design and reward emerging areas of design including business model innovation, social impact and design entrepreneurship.

                    The 60th Anniversary Good Design Awards attracted a record number of entries. From the 536 innovative designs, only 260 projects were selected to receive the coveted Good Design Award®.
                </div>




            </>
        )
    }
}

export default OurWork;