import React from "react";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Components/Nav'
import Product from "./Components/Product";
import Homepage from './Components/Homepage'
import Categories from "./Components/Categories";
import Announcement from "./Components/Announcement";
import WomenCategories from "./Components/WomenCategories";
import MenCategories from "./Components/MenCategories";
import JewelleryCategories from "./Components/JewelleryCategories";
import Gadgets from "./Components/Gadgets";
import AboutUs from './Components/AboutUs'
import OurWork from "./Components/OurWork";
import ShowAllProduct from "./Components/ShowAllProduct";





class App extends React.Component {
  constructor(props) {
    super(props)


  }

  render() {
    return (
      <>
        <Announcement />

        <Routes>

          <Route path="/" element={<Homepage />} />
          <Route path="/products" element={<ShowAllProduct />} />
          <Route path="/collections" element={<Categories />} />
          <Route path="/men-categories" element={<MenCategories />} />
          <Route path="/women-categories" element={<WomenCategories />} />
          <Route path="/jewellery-categories" element={<JewelleryCategories />} />
          <Route path="/electronics-categories" element={<Gadgets />} />
          <Route path="/aboutus" element={<AboutUs />} />
          <Route path="/ourwork" element={<OurWork />} />



        </Routes>


      </>
    );


  }


}


export default App;
